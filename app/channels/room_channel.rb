class RoomChannel < ApplicationCable::Channel
  include SessionsHelper

  def subscribed
    stream_from "room_channel"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def speak(data)
  	Message.create! content: data["message"][0], from_id: current_user.id, 
  										to_id: data["message"][1], room_id: data["message"][2]
  end
end
