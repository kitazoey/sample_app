class RelationshipMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.relationship_mailer.notice_when_followed.subject
  #
  def notice_when_followed(user, follower)
  	@user = user
		@follower = follower

    mail to: @user.email, subject: "You are followed by #{@follower.name}"
  end
end
