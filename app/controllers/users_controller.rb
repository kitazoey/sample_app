class UsersController < ApplicationController
  before_action :logged_in_user, only:[:index, :edit, :update, :destroy, :following, :followers]
  before_action :correct_user, only:[:edit, :update]
  before_action :admin_user, only: :destroy

  def index
    @users = User.where("activated = ?", true).paginate(page: params[:page]).name_like(params[:search])
  end

  def show
    @user = User.find(params[:id])
    redirect_to root_url and return unless @user.activated?
    @microposts = @user.microposts.paginate(page: params[:page]).content_like(params[:search])
  end

  def new
  	@user = User.new
  end

  def create
  	@user = User.new(user_params)
  	if @user.save
      @user.send_activation_email
      flash[:info] = "Please check your email to activate your account."
      redirect_to root_path
  	else
  	  render 'new'
  	end
  end

  def edit
  	@user = User.find(params[:id])
  end

  def update
  	@user = User.find(params[:id])
  	if @user.update_attributes(user_params)
  	  flash[:success] = "Profile updated"
  	  redirect_to @user
  	else
  	  flash[:danger] = @user.errors.full_messages
      redirect_to edit_user_path(@user.id)
  	end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User delete"
    redirect_to users_path
  end

  def following
    @title = "Following"
    @user = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    @user = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end

  def room
    @user = User.find(params[:id])
    @room_id = create_room_id(@user, current_user)
    @messages = current_user.real_chat_message(@room_id)
  end

  private

  	def user_params
  	  params.require(:user).permit(:name, :email, :password, :password_confirmation, :followed_notification)
  	end

    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_path) unless current_user?(@user)
    end

    def admin_user
      redirect_to(root_path) unless current_user.admin?
    end

    def create_room_id(first_user, second_user)
      first_user_id = first_user.id.to_s
      second_user_id = second_user.id.to_s
      return "#{first_user_id}-#{second_user_id}"
    end

end
