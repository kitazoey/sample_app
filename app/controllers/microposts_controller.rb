class MicropostsController < ApplicationController
  before_action :logged_in_user, only:[:show, :create, :destroy]
  before_action :correct_user, only:[:destroy]
  before_action :following_user, only:[:show]

  def show
    @micropost = Micropost.find(params[:id])
    @reply = current_user.microposts.build
    @replies = Micropost.only_reply(@micropost.user_id)
  end

  def create
  	@micropost = current_user.microposts.build(micropost_params)
    re = /@([0-9]+){1,}/
    str = @micropost.content.match(re)

    @micropost.in_reply_to = $1 if $1

  	if @micropost.save
  		flash[:success] = "Microposts created!"
  		redirect_to root_path
  	else
  		@feed_items = []
  		render "static_pages/home"
  	end
  end

  def destroy
  	@micropost.destroy
  	flash[:info] = "Micropost deleted"
  	redirect_to request.referrer || root_url
  end

  private

    def micropost_params
    	params.require(:micropost).permit(:content, :picture, :in_reply_to)
    end

    def correct_user
      @micropost = current_user.microposts.find_by(id: params[:id])
      redirect_to root_url if @micropost.nil?
    end

    def following_user
      @micropost = Micropost.find(params[:id])
      @user = User.find_by(id: @micropost.user_id)
      redirect_to users_path unless current_user.following?(@user) || current_user?(@micropost.user)
    end

end
