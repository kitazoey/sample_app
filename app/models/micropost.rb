class Micropost < ApplicationRecord
  include Searchable
  define_like_search :content
  belongs_to :user
  default_scope -> {order(created_at: :desc)}
  mount_uploader :picture, PictureUploader
  validates :user_id, presence: true
  validates :content, presence: true, length:{ maximum: 140}
  validate :picture_size

  def self.only_reply(value)
  	where(in_reply_to: value)
  end

  private

    def picture_size
      if picture.size > 5.megabytes
      	errors.add(:picture, "Should be less than 5 MB")
      end
    end
end
