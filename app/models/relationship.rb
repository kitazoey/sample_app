class Relationship < ApplicationRecord
	belongs_to :follower, class_name: "User"
	belongs_to :followed, class_name: "User"
	validates :followed_id, presence: true
	validates :follower_id, presence: true

  # followed_userへfollowing_userがフォローしたことを通知する
	def self.send_when_followed(user,follower)
		RelationshipMailer.notice_when_followed(user, follower).deliver_now
	end
end
