require 'active_support'

module Searchable
  extend ActiveSupport::Concern
		class_methods do

			def define_like_search(*columns)
				columns.each do |column|
				  scope :"#{column}_like", ->(search) do
				  	return all unless search
					  where(["#{column} LIKE ?", "%#{search}%"])
					end
			end

		end
  end
end