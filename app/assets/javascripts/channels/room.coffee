App.room = App.cable.subscriptions.create "RoomChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    # Called when there's incoming data on the websocket for this channel
    $("#messages").append data["message"]

  speak: (message) -> 
    @perform 'speak', message: message


$(document).on 'keypress', '[data-behavior~=room_speaker]', (event) ->
  if event.keyCode is 13 # return = send
    App.room.speak [event.target.value, $('[data-user]').attr('data-user'), $('[data-room]').attr('data-room')]
    event.target.value = ''
    event.preventDefault()

$(document).on 'click', '.chat_sendbtn',(event) ->
  App.room.speak [$('#text_id').val(), $('[data-user]').attr('data-user'), $('[data-room]').attr('data-room')]
  event.preventDefault()