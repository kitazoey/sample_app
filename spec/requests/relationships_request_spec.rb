require 'rails_helper'

RSpec.describe "Relationships", type: :request do
	let!(:user){create(:user)}

	context "when not logged in user" do
		it "post method redirect to login url" do
			post relationships_path
			expect(response).to redirect_to login_url
		end

		it "delete method redirect to login url" do
			delete relationship_path(user)
			expect(response).to redirect_to login_url
		end
	end

	context "when login user" do
		let!(:other_user){create(:other_user, followed_notification: true)}
		let(:relation){user.active_relationships.find_by(followed_id: other_user.id)}
		let(:unfollow){delete relationship_path(relation)}
		let(:follow){post relationships_path, params: {followed_id: other_user.id}}

		before do
			log_in_as(user)
		end

		it "follow other user with ajax" do
			expect{follow}.to change(user.following, :count).by(1)
			expect(ActionMailer::Base.deliveries.size).to eq 1
		end

		it "unfollow other user with ajax" do
			follow
			expect{unfollow}.to change(user.following, :count).by(-1)
		end
	end
end
