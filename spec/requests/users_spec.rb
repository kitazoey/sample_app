require 'rails_helper'

RSpec.describe "Users Index", type: :request do
  let!(:user){create(:user)}

  describe "Get #index" do
  	context "when not logged in" do
  	  it "redirect login path" do
  	  	get users_path
  	  	expect(response).to redirect_to login_path
  	  end
  	end
  end

  describe "Get following" do
  	context "when not logged in" do
  		
  		it "access following page redirect login path" do
  			get following_user_path(user)
  			expect(response).to redirect_to login_path
  		end

  		it "access followers page redirect login path" do
  			get followers_user_path(user)
  			expect(response).to redirect_to login_path
  		end
  	end
  end

  describe "Get room" do
    let!(:other_user){create(:other_user)}

    before do
      log_in_as(other_user)
    end

    it "room_id create" do
      get room_user_path(user)
      expect(assigns(:room_id)).to eq "#{user.id}-#{other_user.id}"
    end
  end
end
