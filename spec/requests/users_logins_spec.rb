require 'rails_helper'

RSpec.describe "UsersLogins", type: :request do
  let!(:user){create(:user)}
  
  describe "Post /login" do
    it "login with remembering" do
      log_in_as(user, remember_me:"1")
      expect(cookies[:remember_token]).not_to be nil 
    end

    it "login without remembering" do
      log_in_as(user, remember_me:"1")
      delete logout_path
      log_in_as(user, remember_me:"0")
      expect(cookies[:remember_token]).to be_empty
    end
  end
end
