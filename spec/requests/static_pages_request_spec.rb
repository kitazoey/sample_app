require 'rails_helper'

RSpec.describe "StaticPages", type: :request do
  let!(:base_title){"Ruby on Rails Tutorial Sample App"}

  describe "Root page" do
    it "returns http success" do
      get root_path
      expect(response).to have_http_status(:success)
      assert_select "title", base_title
    end

    it "layouts link correct" do
      get root_path
      assert_template "static_pages/home"
      assert_select "a[href=?]",root_path, count:2
      assert_select "a[href=?]",help_path
      assert_select "a[href=?]",about_path
      assert_select "a[href=?]",contact_path
    end
  end

end
