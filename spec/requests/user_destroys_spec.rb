require 'rails_helper'

RSpec.describe "UserDestroys", type: :request do
  let!(:user){create(:user)}
  let!(:other_user){create(:other_user)}

  describe "delete #destroy" do
  	context "when logged in admin user" do
  	  before do
  	  	log_in_as(user)
  	  end

  	  it "delte other_user" do
  	  	expect do
  	  	  delete user_path(other_user)
  	  	end.to change(User, :count).by(-1)
  	  end
  	end

  	context "when not admin user" do
  	  before do
  	  	log_in_as(other_user)
  	  end

  	  it "can not delte user" do
  	  	delete user_path(user)
  	  	expect(response).to redirect_to root_path
  	  end
  	end
  end
end
