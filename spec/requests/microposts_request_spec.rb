require 'rails_helper'

RSpec.describe "Microposts", type: :request do
  let(:user){create(:user)}

  describe "Get #show" do
    before do
      log_in_as(user)
    end

    context "when not following user" do
      let(:not_following_user){create(:other_user)}
      let!(:not_following_micropost){create(:micropost, user_id:not_following_user.id)}

      it "redirect to Users page" do
        get micropost_path(not_following_micropost.id)
        expect(response).to redirect_to users_url
      end
    end

    context "when following user" do
      let(:following_user){create(:other_user)}
      let!(:following_micropost){create(:micropost, user_id:following_user.id)}

      before do
        follow_user(user, following_user)
      end

      it "response to success" do
        get micropost_path(following_micropost.id)
        expect(response).to have_http_status(:success)
      end
    end
  end

  describe "Post #create" do
  	let(:create_micropost){ post microposts_path, params:{micropost:{content: "Lorem ipsum", picture: picture}}}
    let(:picture){"spec/fixtures/rails.png"}

  	context "when not logged in user" do
  	  it "should be redirect to login path" do
  	  	expect{create_micropost}.to change(Micropost, :count).by(0)
  	  	expect(response).to redirect_to login_url
  	  end
  	end

    context "when log in user" do
      before do
        log_in_as(user)
      end

      it "create micropost" do
        expect{create_micropost}.to change(Micropost, :count).by(1)
        expect(response).to redirect_to root_url
      end
    end
  end

  describe "Delete #destroy" do
    let!(:micropost){create(:micropost, user_id: user.id)}
    let(:destroy_micropost){delete micropost_path(micropost)}
  	context "when not logged in user" do
  	  it "should be redirect to login path" do
  	  	expect{destroy_micropost}.to change(Micropost, :count).by(0)
  	  	expect(response).to redirect_to login_url
  	  end
  	end

    context "when log in user" do
      before do
        log_in_as(user)
      end

      it "destroy micropost" do
        expect{destroy_micropost}.to change(Micropost, :count).by(-1)
        expect(response).to redirect_to root_url
      end
    end
  end
end
