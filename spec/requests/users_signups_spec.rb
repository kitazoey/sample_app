require 'rails_helper'

RSpec.describe "UsersSignups", type: :request do
  describe "Post #create" do
  	let!(:user){create(:user, activated: false)}
    context "when not activated" do
		  it "invalid login" do
		    expect(user.activated?).to eq false
		    log_in_as(user)
		    expect(is_logged_in?).not_to eq nil
		    expect(response).to redirect_to root_path
		  end
    end

    context "when activation_token is invalid" do
    	it "invalid login" do
	    	get edit_account_activation_path("invalid token", email: user.email)
	    	expect(is_logged_in?).not_to eq nil
      end
    end

    context "when activation_token is correct but email is wrong" do
    	it "invalid login" do
    		get edit_account_activation_path(user.activation_token, email: "wrong")
    		expect(is_logged_in?).not_to eq nil
    	end
    end

    context "when correct user email and activation_token" do
    	it "valid signup " do
    		get edit_account_activation_path(user.activation_token, email: user.email)
    		expect(user.reload.activated?).to eq true
    		expect(response).to redirect_to user
    		expect(is_logged_in?).to eq true
    	end
    end
  end
end
