require 'rails_helper'

RSpec.describe "UsersEdits", type: :request do
  let!(:user){create(:user)}
  let!(:other_user){create(:other_user)}

  describe "Get #edit" do
    context "when not logged in" do
      it "can not access edit path redirect_to login_path" do
        get edit_user_path(user.id)
        expect(response).to redirect_to login_path
      end
    end

    context "when other_user get user edit page" do
      it "can not access user edit page redirect root path" do
        log_in_as(other_user)
        get edit_user_path(user.id)
        expect(response).to redirect_to root_path
      end
    end

    context "when correct user logged in" do
      it "successful edit with friendly forwarding" do
        get edit_user_path(user.id)
        expect(session[:forwarding_url]).to eq edit_user_url(user.id) 
        log_in_as(user)
        expect(response).to redirect_to edit_user_path(user.id)
        expect(session[:forwarding_url]).to be nil
      end
    end
  end

  describe "Patch #update" do
    context "when not logged in" do
      it "can not update redirect login path" do
        patch user_path(user), params: {user: {name: user.name,
                email: user.email}}
        expect(response).to redirect_to login_path
      end
    end

    context "when other_user update user profile" do
      it "can not update redirect root path" do
        log_in_as(other_user)
        patch user_path(user), params: {user: {name: user.name,
                email: user.email}}
        expect(response).to redirect_to root_path
      end
    end
  end 
end
