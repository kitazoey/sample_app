require 'rails_helper'

RSpec.describe "PasswordResets", type: :request do
  let(:user){create(:user)}

  describe "Post /passwordresets" do

  	before do
      ActionMailer::Base.deliveries.clear
  		get new_password_reset_path
  	end

  	context "when invalid information" do
	    it "invalid passwordresets email" do
	    	post password_resets_path, params: {password_reset: {email: ""}}
	    	expect(flash[:danger]).to be_present
	    	expect(response).to render_template("password_resets/new")
      end
    end

    context "when valid information" do
    	it "send to user_email & user have reset_digest" do
        old_digest = user.reset_digest
    		post password_resets_path, params: {password_reset: {email: user.email}}
    		expect(user.reload.reset_digest).not_to eq old_digest
    		expect(ActionMailer::Base.deliveries.size).to eq 1
    		expect(flash[:info]).to be_present
    		expect(response).to redirect_to root_path
    	end
    end
  end

  describe "get edit/password_reset" do
    let(:reset_params){{ password_reset:{ email: user.email}}}

  	before do
      ActionMailer::Base.deliveries.clear
  		post password_resets_path, params: reset_params
  	end

  	context "when invalid user information" do
  		it "invalid email redirect to root" do
        user = assigns(:user)
        get edit_password_reset_path(user.reset_token, email: "")
  			expect(response).to redirect_to root_path
  		end

  		it "unactive user redirect to root" do
        user = assigns(:user)
  			get edit_password_reset_path("invalid token", email: user.email)
  			expect(response).to redirect_to root_path
  		end
  	end

  	context "when valid information" do
  		it "get edit password_reset path" do
        user = assigns(:user)
  			get edit_password_reset_path(user.reset_token, email: user.email)
  			expect(response).to render_template("password_resets/edit")
  		end
  	end

    describe "Patch update" do
      context "when invalid password" do
        it "render edit and flash error message" do
          user = assigns(:user)
          patch password_reset_path(user.reset_token), 
                        params: {email: user.email,
                            user: { password: "",
                               password_confirmation: ""}}
          expect(user.errors.add(:password, :blank)).to be_present
        end
      end

      context "when valid password" do
        it "log in user and present flash" do
          user = assigns(:user)
          patch password_reset_path(user.reset_token), 
                        params: {email: user.email,
                            user: { password: "foobaz",
                               password_confirmation: "foobaz"}}
          expect(flash[:success]).to be_present
          expect(response).to redirect_to user
          expect(is_logged_in?).to eq true
        end
      end
    end
  end
end
