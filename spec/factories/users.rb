FactoryBot.define do
  factory :user do
    name { "michel" }
    email { "michel@example.com" }
    password {"password"}
    password_confirmation {"password"}
    admin {true}
    activated {true}
    activated_at {Time.zone.now}
    followed_notification {false}
  end

  factory :other_user, class: User do
    name { "alice" }
    email { "alice@example.com" }
    password {"password"}
    password_confirmation {"password"}
    admin {false}
    activated {true}
    activated_at {Time.zone.now}
  end

  sequence :name do |n|
    "example_#{n}"
  end

  sequence :email do |n|
    "example_#{n}@example.com"
  end
  
  factory :user_list, class: User do
    name
    email
    password {"password"}
    password_confirmation {"password"}
    admin {false}
    activated {true}
    activated_at {Time.zone.now}
  end
end
