FactoryBot.define do
  factory :message do
    content { "MyText" }
    from_id { 1 }
    to_id { 2 }
    room_id {1-1}
  end
end
