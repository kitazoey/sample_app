FactoryBot.define do
  factory :micropost do
    content { "MyText" }
    user { nil }
    created_at  {Time.zone.now}
  end

  sequence :content do |n|
    "MyText_#{n}"
  end

  sequence :created_at do |n|
    n.hours.ago
  end

  factory :micropost_list, class: Micropost do
  	content
  	user {nil}
  	created_at {3.hours.ago}
  end
end
