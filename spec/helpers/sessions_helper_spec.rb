require 'rails_helper'

RSpec.describe SessionsHelper, type: :helper do
  describe "#current_user" do
  	let!(:user){create(:user)}

  	before do
  	  remember(user)
  	end

  	context "when session is nil" do
  	  it "current_user returns right user" do
  	  	expect(current_user).to eq user
  	  	expect(is_logged_in?).not_to eq nil
  	  end
  	end

  	context "when remember digest is wrong" do
  		it "current_user returns nil " do
  			user.update_attribute(:remember_digest, User.digest(User.new_token))
  			expect(current_user).to eq nil
  		end
  	end
  end
end
