require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "full_title" do
    it "returns base_title" do
      expect(helper.full_title).to eq ("Ruby on Rails Tutorial Sample App")
    end
    it "return product title" do
      expect(helper.full_title('About')).to eq ("About | Ruby on Rails Tutorial Sample App")
    end
  end
end
