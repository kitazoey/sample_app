require 'rails_helper'

RSpec.describe Relationship, type: :model do
  let!(:user){create(:user)}
  let!(:other_user){create(:other_user)}
  let(:relationship){create(:relationship, follower_id: user.id, followed_id: other_user.id)}

  it "should be valid" do
  	expect(relationship).to be_valid
  end

  context "when followed_id or follower_id is nil" do
  	it "invalid relationship brcouse follower_id is nil" do
  		relationship.follower_id = nil
  		expect(relationship).not_to be_valid
  	end

  	it "invalid relationship brcouse followed_id is nil" do
  		relationship.followed_id = nil
  		expect(relationship).not_to be_valid
  	end
  end	
end
