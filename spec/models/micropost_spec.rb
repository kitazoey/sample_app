require 'rails_helper'

RSpec.describe Micropost, type: :model do
  describe "micropost" do
  	let(:user){create(:user)}
  	let!(:micropost_list){create_list(:micropost_list, 10, user_id: user.id)}
  	let!(:micropost){create(:micropost, user_id: user.id)}

  	it "should be valid" do
  	  expect(micropost).to be_valid
  	end

  	it "user id should be present" do
  		micropost.user_id = nil
  		expect(micropost).not_to be_valid
  	end

  	it "order shouuld be most rescent first" do
  		expect(micropost).to eq Micropost.first
  	end

  	context "when validates information" do
		  it "email should be invalid" do
		    micropost.content = " "
		    expect(micropost).not_to be_valid
		  end

		  it "password should be invalid" do
		    micropost.content = "a" * 141
		    expect(micropost).not_to be_valid
		  end	
		end


  end
end
