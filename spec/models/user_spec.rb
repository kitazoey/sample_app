require 'rails_helper'

RSpec.describe User, type: :model do
  describe "User model" do
    let!(:user){create(:user)}
    
    describe "validates" do
	    context "when correct user model" do
	      it "should be valid" do
	        expect(user).to be_valid
	      end
	    end
	    
	    context "when present column" do
		    it "name should be invalid" do
		      user.name = " "
		      expect(user).not_to be_valid
		    end

			  it "email should be invalid" do
			    user.email = " "
			    expect(user).not_to be_valid
			  end

			  it "password should be invalid" do
			    user.password = user.password_confirmation = " " * 6
			    expect(user).not_to be_valid
			  end	  
	    end

		  context "when wrong length column" do
			  it "invalid name too long" do
			    user.name = "a" * 51
			    expect(user).not_to be_valid
			  end

			  it "invalid email too long" do
			    user.email = "a" * 244 + "@example.com"
			    expect(user).not_to be_valid
			  end

			  it "invalid password too minimum" do
			    user.password = user.password_confirmation = "a" * 5
			    expect(user).not_to be_valid
			  end	 
		  end

		  context "when email wrong format" do
		  	it "reject invalid address" do
		  	  invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
		  								foo@bar_baz.com foo@bar+baz.com foo@bar..com]
		  	  invalid_addresses.each do |invalid_address|
		  		  user.email = invalid_address
		  	    expect(user).not_to be_valid 
		  	  end
		  	end
		  end

		  context "when email address duplicate" do
				let!(:duplicate_user){user.dup}

				it "invalid email address" do
				  duplicate_user.email = user.email.upcase
				  user.save
				  expect(duplicate_user).not_to be_valid 
				end
		  end

		  context "when email address lower-case" do
				let(:mixed_caxse_email){create(:user, name: "foo", email: "Foo@ExAMPle.CoM")}

				it "invalid mixed_caxse_email" do
				  mixed_caxse_email.save
				  expect(mixed_caxse_email.email).to eq "foo@example.com"
				end
		  end
		end
	  describe "#authenticated" do
	  	it "should be return false for a user with nil digest" do
	  	  expect(user.authenticated?(:remember, "")).to eq false
	  	end
	  end

	  describe "has_many" do
	  	let!(:micropost_list1){create_list(:micropost_list, 5, user_id: user.id)}
	  	let!(:user_second){create(:other_user)}

	  	context "micropost" do
		  	it "destroy user with micropost" do
		  		expect{user.destroy}.to change(Micropost,:count).by(-5)
		  	end
		  end

		  context "following" do
		  	it "user follow other_user & unfollow" do
		  		expect(user.following?(user_second)).to be false
		  		user.follow(user_second)
		  		expect(user.following?(user_second)).to be true
		  		expect(user_second.followers.include?(user)).to be true
		  	  user.unfollow(user_second)
		  	  expect(user.following?(user_second)).to be false
		  	end
		  end

		  describe "feed" do
		  	let!(:user_third){create(:other_user, name: "bob", email: "bob@example.com")}
		  	let!(:micropost_list2){create_list(:micropost_list, 4, user_id: user_second.id)}
		  	let!(:micropost_list3){create_list(:micropost_list, 3, user_id: user_third.id)}

		  	before do
		  		user.follow(user_third)
		  	end

		  	it "check micropost follow user " do
		  		user_third.microposts.each do |post_following|
		  			expect(user.feed).to include post_following
		  		end
		  	end

		  	it "check micropost own" do
		  		user.microposts.each do |post_self|
		  			expect(user.feed).to include post_self
		  		end
		  	end

		  	it "check micropost own" do
		  		user_second.microposts.each do |post_unfollowing|
		  			expect(user.feed).not_to include post_unfollowing
		  		end
		  	end

		  end
	  end
  end
end

