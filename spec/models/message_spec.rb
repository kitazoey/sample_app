require 'rails_helper'

RSpec.describe Message, type: :model do
	describe "Message" do
		let!(:user){create(:user)}
		let!(:other_user){create(:other_user)}
		let(:message){create(:message, from_id: user.id, to_id: other_user.id)}

  	it "should be valid" do
  	  expect(message).to be_valid
  	end

  	context "when validates information" do
		  it "content presence should be invalid" do
		    message.content = " "
		    expect(message).not_to be_valid
		  end

		  it "content 50 length over should be invalid" do
		    message.content = "a" * 51
		    expect(message).not_to be_valid
		  end	

    	it "user id should be present" do
	  		message.from_id = nil
	  		expect(message).not_to be_valid
	  	end

	  	it "user id should be present" do
	  		message.to_id = nil
	  		expect(message).not_to be_valid
	  	end
		end

  end
end
