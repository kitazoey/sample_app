module LoginMacros

  #feature spec
  def login(user)
    visit login_path
  	fill_in "Email", with: user.email
    fill_in "Password", with: user.password
    click_button "Log in"
    expect(current_path).to eq user_path(user.id)
  end

  def logout
    click_link "Log out"
    expect(page).to have_content "Success Logout"
  end

  #request & controller spec 
  def log_in_as(user, password: 'password', remember_me: '1')
    post login_path, params: { session: { email: user.email,
                                          password: password,
                                          remember_me: remember_me } }
  end
  
  def is_logged_in?
    !session[:user_id].nil?
  end
end