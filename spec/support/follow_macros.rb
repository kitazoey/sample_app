module FollowMacros

	def follow_user(following, follower)
		following.follow(follower)
		follower.follow(following)
	end

end