require 'rails_helper'

RSpec.feature "UsersIndices", type: :feature do
  describe "visit Users index page" do
	  let!(:user){create(:user)}
	  let!(:user_list){create_list(:user_list, 30)}

		before do
		  visit login_path
		  login(user)
		  click_link "Users"
		end 

    context "when logged in" do
      it "index including pagination" do
      	expect(current_path).to eq users_path
      	expect(page).to have_selector("div.pagination", count: 2)
        User.paginate(page: 1).each do |u|
          expect(page).to have_css("li", text: u.name)
        end
      end
    end

    describe "search user" do
      let!(:search_user){create(:other_user, name: "Alice")}
      
      it "search user keyword is Alice" do
        fill_in "search", with: "Alice"
        click_button("search")
        expect(page).to have_content search_user.name
       end 
    end
  end
end
