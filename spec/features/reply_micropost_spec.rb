require 'rails_helper'

RSpec.feature "ReplyMicroposts", type: :feature do
  describe "reply" do
   	let(:user){create(:user)}
   	let(:other_user){create(:other_user)}
    let!(:micropost){create(:micropost, content: "test content" , user_id: user.id)}

    before do
      visit login_path
      login(other_user)
      follow_user(user,other_user)
      visit micropost_path(micropost.id)
    end

    context "when create reply" do
    	it "create reply_micropost redirect micropost page" do
    		fill_in "reply message", with: "sounds good!"
    		click_button "Post"
    		expect(page).to have_content "Microposts created!"
        expect(page).to have_content "sounds good"
    	end
    end
  end
end
