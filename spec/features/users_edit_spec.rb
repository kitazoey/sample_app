require 'rails_helper'

RSpec.feature "UsersEdits", type: :feature do
  describe "#Edit" do
  	let!(:user){create(:user)}

		before do
		  visit login_path
		  login(user)
		  click_link "Setting"
		end  	

  	context "when wrong information" do
  	  it "presence password" do
        fill_in "Name", with: ""
        fill_in "Email", with: ""
  	  	click_button "Save changes"
  	  	expect(page).to have_content "Name can't be blank"
        expect(page).to have_content "Email can't be blank"
        expect(current_path).to eq edit_user_path(user.id)
  	  end
  	end

  	context "when correct information" do
  	  it "successful edit" do
        old_name = user.name
        old_email = user.email
        fill_in "Name", with: "Foobar"
        fill_in "Email", with: "Foobar@example.com"
        check('If you want followed notification')
        click_button "Save changes"
        expect(current_path).to eq user_path(user.id)
        user.reload
        expect(user.reload.name).not_to eq old_name
        expect(user.reload.email).not_to eq old_email
      end
  	end
  end
end
