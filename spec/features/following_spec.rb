require 'rails_helper'

RSpec.feature "Followings", type: :feature do
  let(:user){create(:user)}
  let(:other_user){create(:other_user)}
  let(:third_user){create(:other_user, name:"bob", email:"bob@example.com")}

  before do
  	login(user)
    follow_user(user,other_user)
  	other_user.follow(third_user)
  	third_user.follow(user)
  end

  it "display following users" do
  	visit following_user_path(user)
  	expect(user.following.empty?).to be false
	  expect(page).to have_content user.following.count.to_s
    user.following.each do |u|
      expect(page).to have_link u.name
    end
  end

  it "display followers users" do
  	visit followers_user_path(user)
  	expect(user.followers.empty?).to be false
	  expect(page).to have_content user.followers.count.to_s
    user.followers.each do |u|
      expect(page).to have_link u.name
    end
  end
end
