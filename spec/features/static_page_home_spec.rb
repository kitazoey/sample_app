require 'rails_helper'

RSpec.feature "StaticPageHomes", type: :feature do
  let!(:user){create(:user)}
  let!(:micropost){create(:micropost, content: "test content", user_id: user.id)}

  before do
    visit login_path 
    login(user)
    visit root_path
  end

  describe "search micropost" do
  	let!(:micropost_list){create_list(:micropost_list, 40, content: "test list", user_id: user.id)}

  	it "search micropost keyword is content" do
      fill_in "search", with: "content"
      click_button("search")
      expect(page).to have_content micropost.content
  	end
  end

  describe "feed" do
    let!(:reply_user){create(:other_user)}
    let!(:non_reply_user){create(:other_user, name: "bob", email: "bob@example.com")}
    let!(:reply){create(:micropost, content: "test reply", user_id: reply_user.id, in_reply_to: user.id)}

    before do
      follow_user(user, reply_user)
      follow_user(user, non_reply_user)
      follow_user(reply_user, non_reply_user)
      click_link "Home"
    end

    it "reply_user & user only display reply" do
      expect(page).to have_content reply.content
      click_link "Log out"
      visit login_path 
      login(reply_user)
      visit root_path
      expect(page).to have_content reply.content
    end

    it "non_reply_user not display reply" do
      expect(page).to have_content reply.content
      click_link "Log out"
      visit login_path 
      login(non_reply_user)
      visit root_path
      expect(page).not_to have_content reply.content
    end
  end
end
