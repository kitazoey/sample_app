require 'rails_helper'

RSpec.feature "UsersLogins", type: :feature do
  describe "Session" do
  	let!(:user){create(:user)}
  	let!(:flash_msg){"Invalid email/password combination"}

  	before do
  	  visit login_path
  	end

  	context "when invalid information" do
  	  it "presence information" do
  	  	fill_in "Email", with: ""
  	  	fill_in "Password", with: ""
  	  	click_button "Log in"
		    expect(page).to have_content flash_msg
  	  	click_link "Home"
  	  	expect(page).not_to have_content flash_msg
  	  end
  	end

    context "when valid information" do

      before do
        login(user)
      end

      it "header link is correct" do
        within(:css, ".navbar-nav") do
          expect(page).to have_link "Log out"
          expect(page).to have_link "Profile"
          expect(page).to have_link "Setting"
        end
      end

      it "login successes followed by logout" do 
        within(:css, ".dropdown") do
          expect(page).to have_link "Log out"
        end
        logout
        expect(page).not_to have_link "Log out"
        expect(page).to have_link "Log in"
        expect(current_path).to eq root_path
        page.driver.submit :delete, "/logout",{}
      end
    end
  end
end
