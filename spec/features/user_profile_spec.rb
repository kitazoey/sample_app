require 'rails_helper'

RSpec.feature "UserProfiles", type: :feature do
	describe "user_profile" do
  	let(:user){create(:user)}
    let!(:micropost){create(:micropost, content: "test content" , user_id: user.id)}
    let!(:micropost_list){create_list(:micropost_list, 40, content: "test list" , user_id: user.id)}

    before do
      visit login_path
      login(user)
    end

  	context "when user profile display" do
  		it "display only 30 content " do
  			within(:css, ".col-md-8") do
	  			expect(page).to have_content user.name
	  			expect(page).to have_content user.microposts.count.to_s
	      	expect(page).to have_selector("div.pagination", count: 1)
	        user.microposts.paginate(page: 1).each do |m|
	          expect(page).to have_css(".content", text: m.content)
	        end
        end
  		end
  	end

    context "when follow other_user" do
      let(:other_user){create(:other_user)}

      it "display following user count" do
        user.follow(other_user)
        expect(page).to have_content user.following.count.to_s
        expect(page).to have_content user.followers.count.to_s
      end
    end

    describe "search micropost" do
    	it "search micropost keyword is content" do
        fill_in "search", with: "content"
        click_button("search")
        expect(page).to have_content micropost.content
    	end
    end
  end
end
