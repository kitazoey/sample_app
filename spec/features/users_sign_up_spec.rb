require 'rails_helper'

RSpec.feature "UsersSignUps", type: :feature do
  describe "Sign up" do

  	before do
  	  visit signup_path
      ActionMailer::Base.deliveries.clear
  	end

  	context "when wrong status" do
  	  it "invalid sign_up" do
  	  	fill_in "Name", with: "Alice"
  	  	fill_in "Email", with: "example@examle.com"
  	  	click_button "Create my account"
  	  	expect(page).to have_content  "Password can't be blank"
  	  end
  	end

    context "when correct status" do
      it "valid sign up with action mailer" do
        fill_in "Name", with: "Alice"
        fill_in "Email", with: "example@examle.com"
        fill_in "Password", with: "foobar"
        fill_in "Confirmation", with: "foobar"
        click_button "Create my account"
        expect(ActionMailer::Base.deliveries.size).to eq 1
      end
    end
  end
end
